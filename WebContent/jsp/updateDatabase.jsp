<!-- This is needed to connect to the database. -->
<%@ include file="../WEB-INF/connect.jsp" %>

<!-- Check if the query variable is not empty. -->
<c:if test="${not empty param.query}">
	<!-- Use JSTL to fetch a result from a query to the database. -->
	<sql:update var="qry" dataSource="${dataSource}">
		<!-- Print out the value of the response. -->
		<c:out value="${param.query}"/>
	</sql:update>
</c:if>