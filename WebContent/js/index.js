/*
 * Declare variables to the page elements to make things look cleaner.
 */

/**
 * The navigation panel JQuery selector variable. This holds
 * the visual navigation panel. Use this whenever attaining
 * the dimensions of the navigation panel.
 */
var navPanel;

/**
 * The navigation panel content JQuery selector variable. This
 * is the container of all the contents inside of the navigation
 * panel.
 */
var navContent;

/**
 * The navigation panel button JQuery selector variable. This
 * is the button that shows up in the corner of the window when
 * the navigation panel is shrank. It allows the user to re-open
 * the navigation panel after it has been shrank.
 */
var navPanelButton;

/**
 * The page-content JQuery selector variable. This contains all
 * of the content that is shown in the main content panel of the
 * page.
 */
var content;

/**
 * The iframe JQuery selector variable. This is the variable used
 * whenever the iframe's src needs to be edited or likewise.
 */
var iframe;

/**
 * The bottom-panel JQuery selector variable. This is the variable
 * used to separate the timeline panel (top panel) from the rest
 * of the page's content.
 */
var bottomPanel;

/**
 * This variable holds the value of the width of the navigation
 * panel. This value stays constant throughout the whole execution
 * of the program, no matter the size of the web-page.
 */
var navWidth;

/**
 * The variable that holds the value of the opacity
 * of the buttons that show up when you hide the
 * timeline-panel or nav-panel.
 */
var buttonFadeOpacity;

/**
 * Store the old source variable for the iframe. This is used to determine
 * whether or not the src attribute of the iframe has been changed.
 */
var oldSrc;

/**
 * Open the navigation Panel if it is closed.
 */
function openNavPanel()
{
    navPanelButton.stop();
    
    navPanelButton.fadeTo("slow", 0.0, function()
    {
        navPanelButton.addClass("invisible");
    });

    navPanel.show();
    navPanel.addClass('visible');
    navPanel.animate({ "margin-left" : 0 }, "slow");
    
    //content.animate({ "margin-left" : navWidth }, "slow");
}

/**
 * Close the navigation Panel if it is open.
 */
function closeNavPanel()
{
    navPanel.animate({ "margin-left" : (-navPanel.outerWidth()) }, "slow", function()
    {
        navPanel.hide();
        
        navPanelButton.removeClass("invisible");
        navPanelButton.fadeTo("slow", buttonFadeOpacity);
        
        navPanel.removeClass('visible');
    });
    
    //content.animate({ "margin-left" : 0 }, "slow");
}

/**
 * Listener for the click events that are triggered on the
 * navigation panel.
 */
var panelListener = function(event)
{
    var target = $(event.target);
    
    /* Make sure that the event was triggered by the component
     * itself, not by any of its children. */
    if (target.is(navPanel) || target.is(navContent))
    {
        // If it is visible, close it. Else, open it.
        if (navPanel.hasClass('visible'))
        {
            closeNavPanel();
        }
        else
        {
            openNavPanel();
        }
    }
};

/**
 * Function that listens to whenever a button is hovered over.
 */
var buttonHoverInListener = function(event)
{
	// The button that was hovered over.
	var target = $(event.target);
    
    // Delegate the button along through the delegation process.
	delegateHoverEvent(target, true);
};

/**
 * Function that listens to whenever a button has stopped being hovered
 * over.
 */
var buttonHoverOutListener = function(event)
{
	// The button that was stopped being hovered over.
	var target = $(event.target);

    // Delegate the button along through the delegation process.
	delegateHoverEvent(target, false);
};

function delegateHoverEvent(target, hoverIn)
{
    /* Make sure that the event was triggered by the component
     * itself, not by any of its children. */
    if (target.is(navPanelButton))
    {
        // Delegate the hover event to the action method.
    	updateButtonOpacity(navPanelButton, navPanel, hoverIn);
    }
    else if (target.is(timelinePanelButton))
    {
        // Delegate the hover event to the action method.
    	updateButtonOpacity(timelinePanelButton, timelinePanel, hoverIn);
    }
};

/**
 * Action method that changes the opacity of a specified button.
 * If the button was hovered into, then change the opacity to 1,
 * else change it to its default semi-transparent value.
 */
function updateButtonOpacity(selector, selectorParent, hoverIn)
{
	// If the button is not visible then exit the method.
	if (selector.hasClass("invisible") || selectorParent.hasClass("visible"))
    {
        return;
    }
	
	// Stop the button from animating.
	selector.stop();
	
	var opacityValue = 0;
	
	if (hoverIn)
	{
		opacityValue = 1;
	}
	else
	{
		opacityValue = buttonFadeOpacity;
	}
	
	// Fade the buttons opacity to the new value.
	selector.fadeTo("slow", opacityValue);
}

/**
 * Perform some operations on the navigation panel's links.
 * If the link starts with the string "contentPanel:" then
 * make that link open inside of the content-panel instead
 * of its own page.
 * 
 * @param children The children of the navigation panel.
 *         i.e. the links.
 */
function hrefToPage(children)
{
	// Loop through each child.
    for (var i = 0; i < children.length; i++)
    {
        var child = $(children[i]);
        
        /*if (child.prop("tagName").toLowerCase() == "div")
        {
        	child = child.children().eq(0);
        }*/
        
        if (child.hasClass("content-panel-link"))
        {
        	var href = child.attr("href");
        	
        	child.attr("href", "Javascript: openPage('" + href + "');");
        }
        
        hrefToPage(child.children());
    }
}

/**
 * Run all of the listener code and other stuff when the
 * page is ready for it.
 */
$(document).ready(function()
{
    /*
     * Assign variables to the page elements to make things look cleaner.
     */
    navPanel            = $("#nav-panel");
    navContent          = $("#nav-panel-content");
    navPanelButton      = $("#nav-panel-button");
    content             = $("#page-content");
    bottomPanel         = $("#bottom-panel");
    iframe              = $("#page-content-iframe");
    
    navWidth            = navPanel.outerWidth();
    
    buttonFadeOpacity   = 0.4;
    
    hrefToPage(navContent.children());
    
    // Wait for a click on the navPanel.
    navPanel.click(panelListener);
    navContent.click(panelListener);
    
    // Wait for the mouse to hover and unhover the Nav Panel Button.
    navPanelButton.hover(buttonHoverInListener, buttonHoverOutListener);
    
    // Wait for a click on the Nav Panel Button.
    navPanelButton.click(function()
    {
        if (navPanelButton.hasClass("invisible"))
        {
            return;
        }
        
        openNavPanel();
    });
    
    oldSrc = iframe.attr("src");
    
    $("body", iframe.contents()).click(function()
    {
    	var newSrc = iframe.attr("src");
    	
    	if (newSrc != oldSrc)
    	{
    		validateURL(newSrc);
    		
    		oldSrc = newSrc;
    	}
    });
    
    openPage("http://www.ecusd7.org/ehs");
});