/**
 * The Divider selector that contains all of the entries that are
 * output.
 */
var entryData;

/**
 * Array containing all of the entries that have been output to the
 * web-page.
 */
var entries;

/**
 * Add an entry to the entries divider. Will dynamically update the
 * divider's content.
 * 
 * @param id The id of the entry.
 * @param author The author that posted the entry.
 * @param title The title of the entry.
 * @param description The description of the entry.
 * @param url The URL to the entry's web-page.
 * @param date The date that the entry was posted.
 * @param time The time that the entry was posted.
 */
function addEntry(id, author, title, description, url, date, time)
{
	// Uppercase first letter, if it is not.
	if (title.charAt(0).toLowerCase() == title.charAt(0))
	{
		title = title.charAt(0).toUpperCase() + title.substring(1);
	}
	if (author == undefined || author.length <= 0)
	{
		author = "Anonymous";
	}
	if (description == undefined || description.length <= 0)
	{
		description = "No description available.";
	}
	
	var htmlTitle = "<b><h2><u><a href=\"" + url + "\">" + title + "</a></u></h2>";
	
	entryData.prepend("<div class=\"rounded-corners entry\">" +
			htmlTitle + "&nbsp&nbsp&nbspBy " +
			author + " on " +
			date + " at " +
			time + "</b><br><hr>" +
			description +
			"</div><br>");
	
	var first = $("#entries div:first");

	first.attr({ "onclick" : "openPage('" + url + "');" });
	first.children().attr({ "onclick" : "openPage('" + url + "');" });
	
	var entry =
    {
		id:     id,
        author: author,
        title:  title,
        description: description,
        date:   date,
        time:   time,
        url:    url
    };
	
	entries.push(entry);
}

/**
 * Get whether or not the specified entry has already been output to
 * the web-page.
 * 
 * @param entry The entry to check.
 * @returns Whether or not the specified entry has already been output.
 */
function containsEntry(entry)
{
	for (var i = 0; i < entries.length; i++)
	{
		if (entries[i] == entry)
		{
			return true;
		}
	}
	
	return false;
}

function updateTimelineEvents()
{
	retrieveTimelineEvents(addEntry);
}

/**
 * Update the timeline with the latest events in the database.
 */
function retrieveTimelineEvents(callback)
{
	requestPage("retrieveEntry", "", "POST",
        function(data, textStatus, jqXHR)
        {
        	if (data.length <= 0)
        	{
        		return;
        	}
        	
            var entries = $.parseJSON(data);
            
            for (var i = 0; i < entries.length; i++)
            {
            	entries[i].description = entries[i].description.replace(/&#39;/g, "'");
            	
                var entry =
                {
                	id:          entries[i].id,
                    author:      entries[i].author,
                    title:       entries[i].title,
                    description: entries[i].description,
                    date:        entries[i].date,
                    time:        entries[i].time,
                    url:         entries[i].url
                };
                
                if (!containsEntry(entry))
                {
                    callback(entry.id, entry.author, entry.title, entry.description, entry.url, entry.date, entry.time);
                }
                else
                {
                    break;
                }
            }
        },
        
        function(XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Failure!! Contact server administrator for assistance.");
            alert(XMLHttpRequest.responseText);
        },
        
        false
    );
}

/**
 * Set up all of the timeline.html interactive data when the page has
 * loaded the html.
 */
function timelineReady()
{
	// Select the entries divider.
	entryData = $("#entries");
	
	// Instantiate an array for the entries.
	entries = new Array();
	
	updateTimelineEvents();
}