var ge;
var iframeShim;

var params =
{
	"other_params": "sensor=false"
};

google.load("earth", "1", params);

function init()
{
	var navPanel  = window.parent.navPanel;
	var navButton = window.parent.navPanelButton;
	
	navPanel.click(function()
    {
		iframeShim.style.display = 'inline';
    });
	navButton.click(function()
    {
		iframeShim.style.display = 'none';
    });
	
	google.earth.createInstance('map3d', initCB, failureCB);
	
	createNativeHTMLButton(10, 10, 50, 50);
}

function initCB(instance)
{
	ge = instance;
	ge.getWindow().setVisibility(true);
	ge.getNavigationControl().setVisibility(ge.VISIBILITY_SHOW);
	
	var lookAt = ge.createLookAt('');
	lookAt.setLatitude(38.7890275);
	lookAt.setLongitude(-89.9747863);
	lookAt.setRange(1000.0);
	
	ge.getView().setAbstractView(lookAt);
}

function failureCB(errorCode)
{
	alert("The Google Earth Web Plugin was unable to load.");
}

/**
 * Create a custom button using the IFRAME shim and CSS sprite technique
 * at the given x, y offset from the top left of the plugin container.
 */
function createNativeHTMLButton(x, y, width, height)
{
	// create an IFRAME shim for the button
	iframeShim                   = document.createElement('iframe');
	//iframeShim.innerHTML         = "<html><head></head><body></body></html>";
	iframeShim.frameBorder       = 0;
	iframeShim.scrolling         = 'no';
	iframeShim.allowtransparency = 'true';
	iframeShim.style.position    = 'absolute';
	iframeShim.style.left        = x + 'px';
	iframeShim.style.top         = y + 'px';
	iframeShim.style.width       = width + 'px';
	iframeShim.style.height      = height + 'px';
	iframeShim.style.display     = 'none';
	iframeShim.style.zIndex      = -10000;
	
	if (navigator.userAgent.indexOf('MSIE 6') < 0)
	{
		iframeShim.src = 'javascript:void(0);';
	}
	
	// add the iframe shim
	//document.getElementById("iframe-container").appendChild(iframeShim);
	document.getElementById("_idlglue_pluginDiv___idlglue_plugin__99").appendChild(iframeShim);
}

google.setOnLoadCallback(init);