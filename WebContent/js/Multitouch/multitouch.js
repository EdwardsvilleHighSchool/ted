var cursorIds = [];

var canvas    = undefined;

var debug     = false;
var tuio;
/**
 * Load another Javascript file be the relative path given.
 * 
 * @param url The relative path to the other Javascript file.
 * @param callback The function to call when the specified script
 * 	has loaded.
 */
function loadScript(url, callback)
{
	// Adding the script tag to the head as suggested before
	var head    = document.getElementsByTagName('head')[0];
	var script  = document.createElement('script');
	script.type = 'text/javascript';
	
	var relativePath = getRelativePath();
	
	script.src  = relativePath + url;
	
	if (callback !== undefined)
	{
		// Then bind the event to the callback function.
		// There are several events for cross browser compatibility.
		script.onreadystatechange = callback;
		script.onload             = callback;
	}
	
	// Fire the loading
	head.appendChild(script);
	
	function getRelativePath()
	{
		var scripts       = head.getElementsByTagName('script');
		
		var postfixString = "multitouch.js";
		
		for (var i = 0; i < scripts.length; i++)
		{
			var child = scripts[i];
			
			if (child.src.toLowerCase().endsWith(postfixString))
			{
				var absolutePath = child.src.substring(0, child.src.lastIndexOf(postfixString));
				
				return absolutePath;//pathRelativeTo(absolutePath, getCurrentURL());
			}
		}
	}
	
	function pathRelativeTo(url, reference)
	{
		var i = 0;
		
		while (i < url.length && i < reference.length && reference.charAt(i) == url.charAt(i))
		{
			i++;
		}
		
		if (i > 0)
		{
			i--;
		}
		
		
	}
	
	function getCurrentURL()
	{
		var scripts = document.getElementsByTagName("script");

		return scripts[scripts.length - 1].src;
	}
}

String.prototype.endsWith = function(suffix)
{
	return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

function tuio_object_add(sid, fid, x, y, a)
{
	
}

function tuio_object_update(sid, fid, x, y, a)
{
	
}

function tuio_object_remove(sid, fid, x, y, a)
{
	
}

function getWidth()
{
	if (window.innerWidth)
	{
		return window.innerWidth;
	}
	else if (document.compatMode == 'CSS1Compat' && document.documentElement && document.documentElement.offsetWidth)
	{
		return document.documentElement.offsetWidth;
	}
	else if (document.body && document.body.offsetWidth)
	{
		return document.body.offsetWidth;
	}
	
	return -1;
}

function getHeight()
{
	if (window.innerHeight)
	{
		return window.innerHeight;
	}
	else if (document.compatMode == 'CSS1Compat' && document.documentElement && document.documentElement.offsetHeight)
	{
		return document.documentElement.offsetHeight;
	}
	else if (document.body && document.body.offsetHeight)
	{
		return document.body.offsetHeight;
	}
	
	return -1;
}

function tuio_cursor_add(sid, fid, x, y)
{
	if (typeof cursorAdded == 'function')
	{
		cursorAdded(sid, x, y);
	}
}

function tuio_cursor_update(sid, fid, x, y)
{
	if (typeof cursorUpdated == 'function')
	{
		cursorUpdated(sid, x, y);
	}
}

function tuio_cursor_remove(sid, fid, x, y)
{
	if (typeof cursorRemoved == 'function')
	{
		cursorRemoved(sid, x, y);
	}
}

/**
 * Get the ID value with the specified SID.
 * 
 * @param sid The SID value to get the ID with.
 * @return The ID value from the SID.
 */
function getIDFromSID(sid)
{
	return cursorIds[sid];
}

function setIDFromSID(sid, id)
{
	cursorIds[sid] = id;
}

function removeSID(sid)
{
	delete cursorIds[sid];
}

function generateID()
{
	var id = 1;
	var key;
	
	for (var i = mapSize(cursorIds) - 1; i >= 0; i--)
	{
		for (key in cursorIds)
		{
			if (arrayHasOwnIndex(cursorIds, key))
			{
				if (id == cursorIds[key])
				{
					id++;
				}
			}
		}
	}
	
	return id;
}

function mapSize(map)
{
	var size = 0;
	var key;
	
	for (key in map)
	{
		if (map.hasOwnProperty(key))
		{
			size++;
		}
	}
	
	return size;
}

function arrayHasOwnIndex(array, prop)
{
    return array.hasOwnProperty(prop) && /^0$|^[1-9]\d*$/.test(prop) && prop <= 4294967294; // 2^32 - 2
}

function tuio_callback(type, sid, fid, x, y, angle)
{
	if (sid >= 0)
	{
		x *= getWidth();
		y *= getHeight();
	
		y += window.screenY;
	}
	
	var id;
	
	if (type == 0 || type == 3)
	{
		id = generateID();
		
		setIDFromSID(sid, id);
	}
	else
	{
		id = getIDFromSID(sid);
		
		if (type == 2 || type == 5)
		{
			removeSID(sid);
		}
	}
	
	if (type == 0)
	{
		tuio_object_add(id, fid, x, y, a);
	}
	else if (type == 1)
	{
		tuio_object_update(id, fid, x, y, a);
	}
	else if (type == 2)
	{
		tuio_object_remove(id, fid, x, y, a);
	}
	else if (type == 3)
	{
		tuio_cursor_add(id, fid, x, y);
	}
	else if (type == 4)
	{
		tuio_cursor_update(id, fid, x, y);
	}
	else if (type == 5)
	{
		tuio_cursor_remove(id, fid, x, y);
	}
}

function insertTUIOApp()
{
	if (debug)
	{
		loadScript("patch.js", ready);
		
		return;
	}
	
	var obj            = document.createElement("object");
	obj.id             = "tuio";
	obj.style.position = "absolute";
	obj.style.left     = "0";
	obj.style.top      = "0";
	obj.style.width    = "500";
	obj.style.height   = "0";
	obj.type           = "application/x-tuio";
	obj.innerHTML      = "Plugin FAILED to load";
	
	document.body.appendChild(obj);
	
	// If the plugin failed to load.
	if (document.all["tuio"].readyState == 4)
	{
		debug = true;
		
		loadScript("patch.js", ready);
		
		document.body.removeChild(obj);
	}
}

function checkScript()
{
	var scripts = document.getElementsByTagName("script");
	var script  = scripts[scripts.length - 1];
	
	if (script.getAttribute("debug") == "true")
	{
		debug = true;
	}
}

checkScript();

function ready()
{
	canvas = document.getElementById("canvas");
	//console.log("ASDF: " + canvas.height);
	//console.log(window.outerHeight - getHeight() - canvas.height);
}

window.addEventListener("load", insertTUIOApp, false);