var mouseDown = false;

var cursor;

function updateCursor(e)
{
	if (mouseDown && e.button == 0)
	{
		triggerEvent(e, mouseDown);
	}
}

function onClick(e)
{
	if (e.button == 0)
	{
		mouseToggled(false);
		
		triggerEvent(e, false);
	}
}

function triggerEvent(e, down)
{
	if (tuio != undefined && tuio.cursors != undefined)
	{
		removeElement(tuio.cursors, cursor);
		
		if (down)
		{
			cursor = { x: e.clientX, y: e.clientY };
			
			tuio.cursors.unshift(cursor);
		}
	}
	
	if (down)
	{
		tuio_callback(4, -1, 0, e.clientX, e.clientY, 0);
	}
}

function onMouseDown(e)
{
	if (e.button == 0)
	{
		if (!mouseDown)
		{
			tuio_callback(3, -1, 0, e.clientX, e.clientY, 0);
		}
		
		mouseToggled(true);
		
		updateCursor(e);
	}
}

function onMouseUp(e)
{
	if (e.button == 0)
	{
		mouseToggled(false);
		
		tuio_callback(5, -1, 0, e.clientX, e.clientY, 0);
	}
}

function onMouseExit()
{
	//mouseToggled(false);
}

function mouseToggled(down)
{
	mouseDown = down;
	
	if (!down && tuio != undefined && tuio.cursors != undefined)
	{
		removeElement(tuio.cursors, cursor);
	}
}

window.onresize = function()
{
	if (typeof pageResized == 'function')
	{
		pageResized(getWidth(), getHeight());
	}
}

function removeElement(array, element)
{
	for (var i = array.length - 1; i >= 0; i--)
	{
		if (array[i] === element)
		{
			array.splice(i, 1);
		}
	}
}

// Disable all selection on the page.
{
	function disableText(e)
	{
		return false;
	}
	
	function reEnable()
	{
		return true;
	}
	
	//For browser IE4+
	document.onselectstart = new Function ("return false");
	
	//For browser NS6
	if (window.sidebar)
	{
		document.onmousedown = disableText;
		document.onclick     = reEnable;
	}
}

// Set up the event handlers for the Desktop mouse.
window.onmousemove = updateCursor;

window.onmousedown = onMouseDown;
window.onmouseup   = onMouseUp;

window.onmouseout  = onMouseExit;