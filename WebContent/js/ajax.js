/**
 * Redirect the current page to the specified URL.
 */
function redirect(url)
{
	document.location = url;
}

function requestPage(url, data, method, success, fail, async)
{
	if (async == undefined)
	{
		async = true;
	}
	
	$.ajax(
	{
		type: method,
		dataType: "text",
		url: url,
		data: data,
		success: success,
		error: fail,
		async: async
	});
}

function isLoggedIn()
{
	var rank = getRank();
	
	return rank != undefined && rank > 0;
}
	
function getRank()
{
	var result = 0;
	
	requestPage("./verifyRole", "role=student&rank=true", "POST",
			function(data, textStatus, jqXHR)
			{
				if (!isNaN(data) && isFinite(data))
				{
					result = data;
				}
				else
				{
					result = -2;
				}
			},
			function(XMLHttpRequest, textStatus, errorThrown)
			{
				result = -1;
			},
			false
	);
	
	return result;
}