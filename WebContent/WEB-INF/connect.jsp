<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>

<!-- Set up the XML file parser. -->
<c:import var="xmlFile" url="${pageContext.request.contextPath}/../serverdata.xml"/>

<!-- Parse the XML file containing the credentials. -->
<x:parse xml="${xmlFile}" var="output"/>

<!-- Set the username variable to the output from the XML file. -->
<c:set var="username">
	<x:out select="$output/MariaDB/username"/>
</c:set>

<!-- Set the password variable to the output from the XML file. -->
<c:set var="password">
	<x:out select="$output/MariaDB/password"/>
</c:set>

<!--
	Set up a dataSource connection to the database through JDBC using
	the credentials we have attained.
 -->
<sql:setDataSource var="dataSource" driver="org.mariadb.jdbc.Driver"
		url="jdbc:mysql://localhost/" user="${username}" password="${password}"
		scope="session"/>
		
<!-- Delete the credentials so they can not be accessed anymore. -->
<c:remove var="username"/>
<c:remove var="password"/>