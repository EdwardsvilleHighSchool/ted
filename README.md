The Edwardsville High School Interactive Display Board.
Also known as TED (Tiger Electronic Display).


TED is a 12-point multi-touch 50 inch LCD TV used to share student projects at EHS. Students have the ability to develop web-pages and web-apps for TED.